#include <leb-display.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

// WiFi
const char* ssid = "Brotherfields";
const char* password = "goldenage";

//MQTT
#define MQTT_SERVER "broker.hivemq.com"
#define MQTT_PORT 1883
#define MQTT_USER ""
#define MQTT_PASS ""
#define MQTT_INTOPIC "lebDisplay/1B1010001/cmnd/display/"
#define MQTT_OUTTOPIC "lebDisplay/1B1010001/tele/"
#define MQTT_CLIENT "1B1010001"

//LEB DISPLAY
#define P_RE  D4
#define P_DE  D4
#define P_SRX D5
#define P_STX D6
#define POWER D1
#define PID 0x03

#define TIME_LENGTH     4
#define SYMBOL_LENGTH   2
#define TEXT_LENGTH     17
#define TEXT_LINES      2

#define POWER_ON_DELAY  1000
#define CLEAN_DELAY     2500
#define POWER_OFF_DELAY 6000

WiFiClient espClient;
PubSubClient client(espClient);
leb_display d_leb(PID, P_SRX, P_STX, P_RE, P_DE, TIME_LENGTH, SYMBOL_LENGTH, TEXT_LENGTH, TEXT_LINES);

void wait(unsigned long int wait_ms) {
  unsigned long int t0 =millis();
  while((millis() -t0) <wait_ms) {
    yield();
  }
}

void power_on() {
  digitalWrite(POWER, HIGH);
}

void power_off() {
  digitalWrite(POWER, LOW);
}
void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  power_on();
  wait(POWER_ON_DELAY);
  Serial.println("cleaning..");
  d_leb.clean();
  Serial.println("cleaned !");
  wait(CLEAN_DELAY);

  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("]: ");
  char json[length];
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    json[i] =payload[i];
  }
  Serial.println();

  for (int i = 0; i < length; i++) {
    Serial.print((char)json[i]);
  }
  Serial.println();

  StaticJsonBuffer<256> jsonBuffer;

  JsonObject& root = jsonBuffer.parseObject(json);

  char timeF[TIME_LENGTH +1] = {" "};
  sprintf(timeF, root["t"]);
  // Serial.print("t:  ");
  // for (int i = 0; i < TIME_LENGTH; i++) {
  //   Serial.print((char)timeF[i]);
  // }
  // Serial.println();

  char symbol[SYMBOL_LENGTH +1] = {" "};
  sprintf(symbol, root["s"]);
  // Serial.print("s:  ");
  // for (int i = 0; i < SYMBOL_LENGTH; i++) {
  //   Serial.print((char)symbol[i]);
  // }
  // Serial.println();

  char line1[TEXT_LENGTH +1] = {" "};
  sprintf(line1, root["l1"]);
  // Serial.print("l1: ");
  // for (int i = 0; i < TEXT_LENGTH; i++) {
  //   Serial.print((char)line1[i]);
  // }
  // Serial.println();

  char line2[TEXT_LENGTH +1] = {" "};
  sprintf(line2, root["l2"]);
  // Serial.print("l2: ");
  // for (int i = 0; i < TEXT_LENGTH; i++) {
  //   Serial.print((char)line2[i]);
  // }
  // Serial.println();

  Serial.println("displaying..");
  d_leb.display(timeF, symbol, line1, line2);
  Serial.println("displayed !");

  wait(POWER_OFF_DELAY);
  Serial.println("Power Off.");
  power_off();
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    // if (client.connect(MQTT_CLIENT, MQTT_USER, MQTT_PASS)) {
    if (client.connect(MQTT_CLIENT)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish(MQTT_OUTTOPIC, "Online");
      // ... and resubscribe
      client.subscribe(MQTT_INTOPIC);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  pinMode(BUILTIN_LED, OUTPUT);
  Serial.begin(115200);
  setup_wifi();
  client.setServer(MQTT_SERVER, MQTT_PORT);
  client.setCallback(callback);
  pinMode(POWER, OUTPUT);
  digitalWrite(POWER, LOW);
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

}
